/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package testframework

import (
	"testing"

	msgbus2 "chainmaker.org/chainmaker/common/v2/msgbus"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
)

//TestBlockCommitterForTest
func TestBlockCommitterForTest(t *testing.T) {
	msgbus := msgbus2.NewMessageBus()
	blockCommit := newBlockCommitterForTest(msgbus)
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:      "chain1",
			BlockHeight:  100,
			PreBlockHash: nil,
		},
		Txs: fetchTxBatch(txNum),
	}
	blockCommit.AddBlock(block)
}
