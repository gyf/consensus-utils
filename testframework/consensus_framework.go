/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package testframework

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"

	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"

	"chainmaker.org/chainmaker/protocol/v2/mock"
	"github.com/golang/mock/gomock"

	"chainmaker.org/chainmaker/common/v2/msgbus"
	"chainmaker.org/chainmaker/localconf/v2"
	"chainmaker.org/chainmaker/logger/v2"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/pb-go/v2/consensus"
	consensusPb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	netPb "chainmaker.org/chainmaker/pb-go/v2/net"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"

	"github.com/gogo/protobuf/proto"
)

const (
	LIMIT_DELIMITER = "/"
	hashType        = "SHA256"
	authType        = "identity"
	configDir       = "./config"

	privateKeyFilePrefix = "./config/%s/node/consensus1/consensus1.sign.key"
	certFilePrefix       = "./config/%s/node/consensus1/consensus1.sign.crt"
	rootFilePrefix       = "./config/%s/ca/ca.crt"
)

var (
	txSize = 2 * 1024
	txNum  = 20 * 1024

	//chain_config                      = &config.ChainConfig{}
	local_config = make(map[string]*localconf.CMConfig) // local_config[orgId] = CMConfig
	memberIds    = []string{
		"42baa5cc60ae451aa9e44812c7894c29",
		"43ea0b97a82945449163348585aff19c",
		"5f294aa59e3c4bde85a8a764723f57ed",
		"fc730a0f3e7241d1b1173a4897af96e7",
	}
	//mapper_certId_nodeId:mapper[certId]=nodeId
	mapper_certId_nodeId = map[string]string{
		"42baa5cc60ae451aa9e44812c7894c29": "QmV9wyvnGXtKauR2MV4bLndwfS4hnHkN6RhXMmEyLyRwqq",
		"43ea0b97a82945449163348585aff19c": "QmYjXpS5RtSiScjJVxzJNUo2XdfDbSoE1BaaSQG2BWLhej",
		"5f294aa59e3c4bde85a8a764723f57ed": "QmYhNgL59EQriiojax98a8HQnB4DPqdN44eRy3RCdgbNPn",
		"fc730a0f3e7241d1b1173a4897af96e7": "Qmd6RRKw83sQrf4oZJEhuhouz48eu9BT1nLKNGqKcpD6LL",
	}
	//org list
	org_s = []string{
		"wx-org1.chainmaker.org",
		"wx-org2.chainmaker.org",
		"wx-org3.chainmaker.org",
		"wx-org4.chainmaker.org",
		"wx-org5.chainmaker.org",
		"wx-org6.chainmaker.org",
		"wx-org7.chainmaker.org",
	}
	//node list
	node_s = []string{
		"QmV9wyvnGXtKauR2MV4bLndwfS4hnHkN6RhXMmEyLyRwqq",
		"QmYjXpS5RtSiScjJVxzJNUo2XdfDbSoE1BaaSQG2BWLhej",
		"QmYhNgL59EQriiojax98a8HQnB4DPqdN44eRy3RCdgbNPn",
		"Qmd6RRKw83sQrf4oZJEhuhouz48eu9BT1nLKNGqKcpD6LL",
		"QmNVStQFk7uPpaBGLr1eZABYfyazfMGTQ6SrXVniZ74W6i",
		"QmYQQsMAdcbkY2U1FER7ZWMhWENBNUZdhUEdSXWpp2H8AK",
		"QmcUDfpj57gJSmtapzpo2TatZSZmFfJSnbGza5cLkweESQ",
	}
	//map_nodeId_num:map[nodeId]index
	map_nodeId_num = map[string]int{
		"QmV9wyvnGXtKauR2MV4bLndwfS4hnHkN6RhXMmEyLyRwqq": 1,
		"QmYjXpS5RtSiScjJVxzJNUo2XdfDbSoE1BaaSQG2BWLhej": 2,
		"QmYhNgL59EQriiojax98a8HQnB4DPqdN44eRy3RCdgbNPn": 3,
		"Qmd6RRKw83sQrf4oZJEhuhouz48eu9BT1nLKNGqKcpD6LL": 4,
		"QmNVStQFk7uPpaBGLr1eZABYfyazfMGTQ6SrXVniZ74W6i": 5,
		"QmYQQsMAdcbkY2U1FER7ZWMhWENBNUZdhUEdSXWpp2H8AK": 6,
		"QmcUDfpj57gJSmtapzpo2TatZSZmFfJSnbGza5cLkweESQ": 7,
	}
)

// ####################################################################################################################
//                                         new ChainConfig, LocalConfig
// ####################################################################################################################

//InitLocalConfigs
func InitLocalConfigs() error {
	return initConfigs(configDir)
}

//RemoveLocalConfigs
func RemoveLocalConfigs() {
	os.RemoveAll(configDir)
}

//SetTxSizeAndTxNum
func SetTxSizeAndTxNum(size, num int) {
	txSize = size
	txNum = num
	fmt.Printf("block size:%dMB\n", (txSize*txNum)/1024/1024)
}

//InitChainConfig
func InitChainConfig(chainId string, consensusType consensusPb.ConsensusType, nodeNum int) *configPb.ChainConfig {
	trustMemConfigs := []*configPb.TrustMemberConfig{}

	for i := 0; i < nodeNum; i++ {
		trustMemConfig := &configPb.TrustMemberConfig{
			MemberInfo: memberIds[i],
			OrgId:      org_s[i],
			Role:       "Member",
			NodeId:     node_s[i],
		}
		trustMemConfigs = append(trustMemConfigs, trustMemConfig)
	}

	cc := &configPb.ChainConfig{
		ChainId:  chainId,
		Version:  "v1.2.0",
		AuthType: authType,
		Sequence: 1,
		Crypto:   &configPb.CryptoConfig{Hash: hashType},
		Block: &configPb.BlockConfig{
			BlockSize: 100,
		},
		Contract: &configPb.ContractConfig{
			EnableSqlSupport: false,
		},
		TrustMembers: trustMemConfigs,
	}

	// new ConsensusConfig
	ns := make([]*configPb.OrgConfig, nodeNum)
	for i := 0; i < nodeNum; i++ {
		ns[i] = &configPb.OrgConfig{
			OrgId:  org_s[i],
			NodeId: []string{node_s[i]},
		}
	}

	cc.Consensus = &configPb.ConsensusConfig{
		Type:      consensusType,
		Nodes:     ns,
		ExtConfig: nil,
	}

	//prepare DPOS config
	if consensusType == consensusPb.ConsensusType_DPOS {
		cc.Consensus.DposConfig = []*configPb.ConfigKeyValue{
			{
				Key:   "erc20.total",
				Value: "10000000",
			},
			{
				Key:   "erc20.owner",
				Value: "4WUXfiUpLkx7meaNu8TNS5rNM7YtZk6fkNWXihc54PbM",
			},
			{
				Key:   "erc20.decimals",
				Value: "18",
			},
			{
				Key:   "erc20.account:DPOS_STAKE",
				Value: "10000000",
			},
			{
				Key:   "stake.minSelfDelegation",
				Value: "2500000",
			},
			{
				Key:   "stake.epochValidatorNum",
				Value: "4",
			},
			{
				Key:   "stake.epochBlockNum",
				Value: "10",
			},
			{
				Key:   "stake.completionUnbondingEpochNum",
				Value: "1",
			},
			{
				Key:   "stake.candidate:6NbgYXzHhgigS8b4215iDiKxwekjkmgb8iXUqTSjC3Cm",
				Value: "2500000",
			},
			{
				Key:   "stake.candidate:3Lg6X7me2Ln2TkQchZwsJb7BRrtqoag4wwHJ2vsbeAoU",
				Value: "2500000",
			},
			{
				Key:   "stake.candidate:5Kn7aB2LLdurbtkrp1Gxvv69FLACroUqnHA2j3Wr1gW6",
				Value: "2500000",
			},
			{
				Key:   "stake.candidate:2LjvZJWcanVankmyzMKiYeoHQeTbsXM7VqG1bUwPfAkS",
				Value: "2500000",
			},
			{
				Key:   "stake.nodeID:6NbgYXzHhgigS8b4215iDiKxwekjkmgb8iXUqTSjC3Cm",
				Value: "QmV9wyvnGXtKauR2MV4bLndwfS4hnHkN6RhXMmEyLyRwqq",
			},
			{
				Key:   "stake.nodeID:3Lg6X7me2Ln2TkQchZwsJb7BRrtqoag4wwHJ2vsbeAoU",
				Value: "QmYhNgL59EQriiojax98a8HQnB4DPqdN44eRy3RCdgbNPn",
			},
			{
				Key:   "stake.nodeID:5Kn7aB2LLdurbtkrp1Gxvv69FLACroUqnHA2j3Wr1gW6",
				Value: "QmYjXpS5RtSiScjJVxzJNUo2XdfDbSoE1BaaSQG2BWLhej",
			},
			{
				Key:   "stake.nodeID:2LjvZJWcanVankmyzMKiYeoHQeTbsXM7VqG1bUwPfAkS",
				Value: "Qmd6RRKw83sQrf4oZJEhuhouz48eu9BT1nLKNGqKcpD6LL",
			},
		}
	}

	// new TrustRootConfig
	cc.TrustRoots = make([]*configPb.TrustRootConfig, nodeNum)
	for i := 0; i < nodeNum; i++ {
		absPath, _ := filepath.Abs(fmt.Sprintf(rootFilePrefix, org_s[i]))
		root, err := ioutil.ReadFile(absPath)
		if err != nil {
			panic(fmt.Errorf("init chainConfig failed, err:%s", err.Error()))
		}
		tr := &configPb.TrustRootConfig{
			OrgId: org_s[i],
			Root:  []string{string(root)},
		}
		cc.TrustRoots[i] = tr
	}
	return cc
}

//InitLocalConfig
func InitLocalConfig(nodeNum int) {
	for i := 0; i < nodeNum; i++ {
		lc := &localconf.CMConfig{}

		lc.NodeConfig.Type = "full"
		lc.NodeConfig.CertFile = fmt.Sprintf(certFilePrefix, org_s[i])
		lc.NodeConfig.PrivKeyFile = fmt.Sprintf(privateKeyFilePrefix, org_s[i])
		lc.NodeConfig.PrivKeyPassword = ""
		lc.NodeConfig.AuthType = authType
		lc.NodeConfig.NodeId = node_s[i]
		lc.NodeConfig.OrgId = org_s[i]
		lc.NodeConfig.SignerCacheSize = 1000
		lc.NodeConfig.CertCacheSize = 1000

		lc.NodeConfig.P11Config.Enabled = false
		lc.NodeConfig.P11Config.Library = ""
		lc.NodeConfig.P11Config.Label = ""
		lc.NodeConfig.P11Config.Password = ""
		lc.NodeConfig.P11Config.SessionCacheSize = 10
		lc.NodeConfig.P11Config.Hash = hashType

		//lc.SetConsensusConfig(10, true)
		localconf.ChainMakerConfig = lc
		local_config[org_s[i]] = lc
	}
}

// ####################################################################################################################
//                                           Cluster Framework
// ####################################################################################################################
type TestClusterFramework struct {
	chainId string
	tns     []*TestNode
}

//NewTestClusterFramework
func NewTestClusterFramework(chainId string, consensusType consensusPb.ConsensusType,
	nodeNum int, tnc []*TestNodeConfig, consensusEngines []protocol.ConsensusEngine,
	coreEngines []protocol.CoreEngine) (*TestClusterFramework, error) {

	// create TestClusterFramework
	tf := &TestClusterFramework{
		chainId: chainId,
		tns:     make([]*TestNode, nodeNum),
	}
	// create test node
	for i := 0; i < nodeNum; i++ {
		var err error
		tf.tns[i], err = NewTestNode(consensusType, tnc[i], tf, consensusEngines[i], coreEngines[i])
		if err != nil {
			return nil, err
		}
	}
	return tf, nil
}

//Start
func (tf *TestClusterFramework) Start() {
	fmt.Println("======== Cluster Framework Start ========")
	for _, tn := range tf.tns {
		tn.Start()
	}
}

//Stop
func (tf *TestClusterFramework) Stop() {
	for _, tn := range tf.tns {
		tn.Stop()
	}
	fmt.Println("======== Cluster Framework Stop ========")
}

// ####################################################################################################################
//                                                  Test Node
// ####################################################################################################################
type TestNode struct {
	chainId      string
	nodeId       string
	genesisBlock *commonPb.Block

	msgBus          msgbus.MessageBus
	coreEngine      protocol.CoreEngine
	consensusEngine protocol.ConsensusEngine
	netEngine       *NetEngineForTest
}

//TestNodeConfig
type TestNodeConfig struct {
	ChainID         string
	NodeId          string
	ConsensusType   consensusPb.ConsensusType
	GenesisBlock    *commonPb.Block
	Signer          protocol.SigningMember
	Ac              protocol.AccessControlProvider
	LedgerCache     protocol.LedgerCache
	ChainConf       protocol.ChainConf
	MsgBus          msgbus.MessageBus
	BlockchainStore protocol.BlockchainStore
	ProposalCache   protocol.ProposalCache
}

//GetConsensusArg
type GetConsensusArg func(cfg *configPb.ChainConfig) []byte

//CreateTestNodeConfig
func CreateTestNodeConfig(ctrl *gomock.Controller, nodeNum int, chainId string,
	consensusType consensus.ConsensusType, fn GetConsensusArg) ([]*TestNodeConfig, error) {

	testNodeConfigs := make([]*TestNodeConfig, nodeNum)

	// create test node
	for i := 0; i < nodeNum; i++ {
		chainconfig := InitChainConfig(chainId, consensusType, nodeNum)
		chainConfigBytes, err := proto.Marshal(chainconfig)
		if err != nil {
			return nil, err
		}
		// todo. the return val should be process.
		var maxbftConsensusVal []byte
		if fn != nil {
			maxbftConsensusVal = fn(chainconfig)
		}

		//mock blockchainStore
		blockchainStore := newMockStore(ctrl, maxbftConsensusVal, chainConfigBytes)
		chainConfigForTest := newChainConfImplForTest(blockchainStore, chainconfig)

		// create genesis block
		genesisBlock, _, err := utils.CreateGenesis(chainconfig)
		if err != nil {
			return nil, fmt.Errorf("create chain [%s] genesis block failed, %s", chainId, err.Error())
		}
		// create ledgerCache
		ledgerCache := NewCache(chainId)
		ledgerCache.SetLastCommittedBlock(genesisBlock)

		proposalCache := NewProposalCache(chainConfigForTest, ledgerCache)
		ac := newMockAccessControl(ctrl, i)

		//nodeConfig := local_config[org_s[i]].NodeConfig
		//create signer
		//signer, err := InitCertSigningMember(chainConfigForTest.ChainConfig(), nodeConfig.OrgId,
		//	nodeConfig.PrivKeyFile, nodeConfig.PrivKeyPassword, nodeConfig.CertFile)
		//if err != nil {
		//	return nil, err
		//}

		//mock signer
		signer := newMockSigner(ctrl, i)

		config := &TestNodeConfig{
			ChainID:         chainId,
			NodeId:          node_s[i],
			ConsensusType:   consensusType,
			GenesisBlock:    genesisBlock,
			Signer:          signer,
			Ac:              ac,
			LedgerCache:     ledgerCache,
			ChainConf:       chainConfigForTest,
			MsgBus:          msgbus.NewMessageBus(),
			BlockchainStore: blockchainStore,
			ProposalCache:   proposalCache,
		}

		testNodeConfigs[i] = config
	}
	return testNodeConfigs, nil
}

//newMockSigner
func newMockSigner(ctrl *gomock.Controller, i int) protocol.SigningMember {
	signer := mock.NewMockSigningMember(ctrl)
	signer.EXPECT().Sign(gomock.Any(), gomock.Any()).Return([]byte("123"), nil).AnyTimes()
	signer.EXPECT().GetMember().DoAndReturn(
		func() (*accesscontrol.Member, error) {
			return &accesscontrol.Member{
				OrgId:      org_s[i],
				MemberType: accesscontrol.MemberType_CERT,
				MemberInfo: []byte(memberIds[i]),
			}, nil
		}).AnyTimes()

	return signer
}

//newMockAccessControl
func newMockAccessControl(ctrl *gomock.Controller, i int) protocol.AccessControlProvider {
	ac := mock.NewMockAccessControlProvider(ctrl)
	ac.EXPECT().CreatePrincipal(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil, nil).AnyTimes()
	ac.EXPECT().VerifyPrincipal(gomock.Any()).Return(true, nil).AnyTimes()
	ac.EXPECT().NewMember(gomock.Any()).DoAndReturn(
		func(acMember *accesscontrol.Member) (protocol.Member, error) {
			member := newMockMember(ctrl, i)
			return member, nil
		}).AnyTimes()
	return ac
}

//newMockMember
func newMockMember(ctrl *gomock.Controller, i int) protocol.Member {
	member := mock.NewMockMember(ctrl)
	member.EXPECT().GetMemberId().Return(memberIds[i]).AnyTimes()
	member.EXPECT().GetMember().Return(&accesscontrol.Member{
		OrgId:      org_s[i],
		MemberType: accesscontrol.MemberType_CERT,
		MemberInfo: []byte(memberIds[i]),
	}, nil).AnyTimes()
	return member
}

//newMockStore
func newMockStore(ctrl *gomock.Controller, maxbftConsensusVal, chainConfVal []byte) protocol.BlockchainStore {
	store := mock.NewMockBlockchainStore(ctrl)
	content := sync.Map{}
	configDbKey := fmt.Sprintf("%s%s",
		syscontract.SystemContract_CHAIN_CONFIG.String(),
		syscontract.SystemContract_CHAIN_CONFIG.String())
	content.Store(configDbKey, chainConfVal)
	if len(maxbftConsensusVal) > 0 {
		consensusDbKey := fmt.Sprintf("%s%s",
			syscontract.SystemContract_GOVERNANCE.String(),
			syscontract.SystemContract_GOVERNANCE.String())
		content.Store(consensusDbKey, maxbftConsensusVal)
	}

	store.EXPECT().ReadObject(gomock.Any(), gomock.Any()).DoAndReturn(
		func(contractName string, key []byte) ([]byte, error) {
			dbKey := fmt.Sprintf("%s%s", contractName, key)
			val, ok := content.Load(dbKey)
			if ok {
				return val.([]byte), nil
			}
			return nil, fmt.Errorf("not find key: %s value", dbKey)
		}).AnyTimes()

	store.EXPECT().PutBlock(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	return store
}

//NewTestNode
func NewTestNode(
	consensusType consensusPb.ConsensusType,
	config *TestNodeConfig,
	tf *TestClusterFramework,
	ce protocol.ConsensusEngine,
	core protocol.CoreEngine) (*TestNode, error) {

	tn := &TestNode{
		chainId:      config.ChainID,
		nodeId:       config.NodeId,
		genesisBlock: config.GenesisBlock,
		msgBus:       config.MsgBus,
	}

	tn.coreEngine = core
	// new newNetEngineForTest
	net := NewNetEngineForTest(config.ChainID, config.NodeId, config.MsgBus, tf)
	tn.netEngine = net

	l := &logger.LogConfig{
		SystemLog: logger.LogNodeConfig{
			FilePath:        fmt.Sprintf("./default.%s.log", config.NodeId),
			LogLevelDefault: "INFO",
			LogLevels:       map[string]string{"consensus": "INFO", "core": "INFO", "net": "INFO"},
			LogInConsole:    false,
			ShowColor:       true,
		},
	}
	logger.SetLogConfig(l)

	// create ConsensusEngine
	switch consensusType {
	case consensus.ConsensusType_TBFT:
		tn.consensusEngine = ce
		return tn, nil

	case consensus.ConsensusType_MAXBFT:
		tn.consensusEngine = ce
		return tn, nil

	case consensus.ConsensusType_RAFT:
		tn.consensusEngine = ce
		return tn, nil

	case consensus.ConsensusType_DPOS:
		tn.consensusEngine = ce
		return tn, nil

	default:
		return nil, errors.New("only support raft tbft and maxbft")
	}
}

//Start
func (tn *TestNode) Start() {
	fmt.Printf("-------- [nodeId:%d,%s] Node Start --------\n", map_nodeId_num[tn.nodeId], tn.nodeId)
	tn.netEngine.Start()
	tn.coreEngine.Start()
	err := tn.consensusEngine.Start()
	if err != nil {
		panic(err)
	}
}

//Stop
func (tn *TestNode) Stop() {
	err := tn.consensusEngine.Stop()
	if err != nil {
		panic(err)
	}

	tn.coreEngine.Stop()
	tn.netEngine.Stop()
	fmt.Printf("-------- [nodeId:%d,%s] Node Stop --------\n", map_nodeId_num[tn.nodeId], tn.nodeId)
}

// ####################################################################################################################
//                                                  Net Engine
// ####################################################################################################################
type NetEngineForTest struct {
	chainId string
	nodeId  string
	msgBus  msgbus.MessageBus
	tf      *TestClusterFramework
	log     *logger.CMLogger
}

//NewNetEngineForTest
func NewNetEngineForTest(chainId, nodeId string, msgBus msgbus.MessageBus, tf *TestClusterFramework) *NetEngineForTest {
	ne := &NetEngineForTest{
		chainId: chainId,
		nodeId:  nodeId,
		msgBus:  msgBus,
		tf:      tf,
		log:     logger.GetLogger(logger.MODULE_NET),
	}
	return ne
}

//Start
func (ne *NetEngineForTest) Start() {
	fmt.Printf("[nodeId:%d,%s] Net Engine Start\n", map_nodeId_num[ne.nodeId], ne.nodeId)
	ne.log.Infof("[nodeId:%d,%s] Net Engine Start", map_nodeId_num[ne.nodeId], ne.nodeId)
	ne.msgBus.Register(msgbus.SendConsensusMsg, ne)
}

//Stop
func (ne *NetEngineForTest) Stop() {
	ne.log.Infof("[nodeId:%d,%s] Net Engine Stop", map_nodeId_num[ne.nodeId], ne.nodeId)
	fmt.Printf("[nodeId:%d,%s] Net Engine Stop\n", map_nodeId_num[ne.nodeId], ne.nodeId)
}

//OnMessage
func (ne *NetEngineForTest) OnMessage(message *msgbus.Message) {
	switch message.Topic {
	case msgbus.SendConsensusMsg:
		if netMsg, ok := message.Payload.(*netPb.NetMsg); ok {
			if netMsg.Type.String() != netPb.NetMsg_CONSENSUS_MSG.String() {
				ne.log.Infof("[nodeId:%d,%s] net msg type is not expected, actual:%s, expected:%s\n",
					map_nodeId_num[ne.nodeId], ne.nodeId, netMsg.Type.String(), netPb.NetMsg_CONSENSUS_MSG.String())
				return
			}
			// broadcast or send
			if netMsg.To == "" {
				ne.log.Infof("[nodeId:%d,%s] broadcast net msg", map_nodeId_num[ne.nodeId], ne.nodeId)
				for _, tn := range ne.tf.tns {
					if ne.nodeId != tn.nodeId {
						tn.msgBus.Publish(msgbus.RecvConsensusMsg, netMsg)
					}
				}
			} else {
				for _, tn := range ne.tf.tns {
					if netMsg.To == tn.nodeId {
						ne.log.Infof("[nodeId:%d,%s] send net msg to [nodeId:%d,%s]",
							map_nodeId_num[ne.nodeId], ne.nodeId, map_nodeId_num[netMsg.To], netMsg.To)
						tn.msgBus.Publish(msgbus.RecvConsensusMsg, netMsg)
					}
				}
			}
		}
	}
}

//OnQuit
func (ne *NetEngineForTest) OnQuit() {
	ne.log.Infof("[nodeId:%d,%s] Net Engine quit", map_nodeId_num[ne.nodeId], ne.nodeId)
}

// ####################################################################################################################
//                                       impls ChainConf interface
// ####################################################################################################################
type ChainConfImplForTest struct {
	ChainConf       *configPb.ChainConfig
	blockchainStore protocol.BlockchainStore
}

//newChainConfImplForTest
func newChainConfImplForTest(store protocol.BlockchainStore, cfg *configPb.ChainConfig) *ChainConfImplForTest {
	return &ChainConfImplForTest{
		ChainConf:       cfg,
		blockchainStore: store,
	}
}

//Init
func (cc *ChainConfImplForTest) Init() error {

	// load chain config from store
	bytes, err := cc.blockchainStore.ReadObject(syscontract.SystemContract_CHAIN_CONFIG.String(),
		[]byte(syscontract.SystemContract_CHAIN_CONFIG.String()))
	if err != nil {
		return err
	}
	if len(bytes) == 0 {
		return errors.New("ChainConfig is empty")
	}
	var chainConfig configPb.ChainConfig
	err = proto.Unmarshal(bytes, &chainConfig)
	if err != nil {
		return err
	}

	cc.ChainConf = &chainConfig
	// compatible with versions before v1.1.1
	if cc.ChainConf.Contract == nil {
		cc.ChainConf.Contract = &configPb.ContractConfig{EnableSqlSupport: false} //by default disable sql support
	}
	return nil
}

//ChainConfig
func (cc *ChainConfImplForTest) ChainConfig() *configPb.ChainConfig {
	return cc.ChainConf
}

//GetChainConfigFromFuture
func (cc *ChainConfImplForTest) GetChainConfigFromFuture(blockHeight uint64) (*configPb.ChainConfig, error) {
	return cc.ChainConf, nil
}

//GetChainConfigAt
func (cc *ChainConfImplForTest) GetChainConfigAt(blockHeight uint64) (*configPb.ChainConfig, error) {
	return cc.ChainConf, nil
}

//GetConsensusNodeIdList
func (cc *ChainConfImplForTest) GetConsensusNodeIdList() ([]string, error) {
	chainNodeList := make([]string, 0)
	for _, node := range cc.ChainConf.Consensus.Nodes {
		//for _, nid := range node.NodeId {
		chainNodeList = append(chainNodeList, node.NodeId...)
		//}
	}
	return chainNodeList, nil
}

//CompleteBlock
func (cc *ChainConfImplForTest) CompleteBlock(block *commonPb.Block) error {
	return nil
}

//AddWatch
func (cc *ChainConfImplForTest) AddWatch(w protocol.Watcher) {}

//AddVmWatch
func (cc *ChainConfImplForTest) AddVmWatch(w protocol.VmWatcher) {}

// ####################################################################################################################
//                                       impls LedgerCache          (avoid circular references to core module)
// ####################################################################################################################
// Cache is used for cache current block info
type Cache struct {
	chainId            string
	lastCommittedBlock *commonPb.Block
	rwMu               sync.RWMutex
}

// NewLedgerCache get a ledger cache.
// One ledger cache for one chain.
func NewCache(chainId string) protocol.LedgerCache {
	return &Cache{
		chainId: chainId,
	}
}

// GetLastCommittedBlock get the latest committed block
func (c *Cache) GetLastCommittedBlock() *commonPb.Block {
	c.rwMu.RLock()
	defer c.rwMu.RUnlock()
	return c.lastCommittedBlock
}

// SetLastCommittedBlock set the latest committed block
func (c *Cache) SetLastCommittedBlock(b *commonPb.Block) {
	c.rwMu.Lock()
	defer c.rwMu.Unlock()
	c.lastCommittedBlock = b
}

// CurrentHeight get current block height
func (c *Cache) CurrentHeight() (uint64, error) {
	c.rwMu.RLock()
	defer c.rwMu.RUnlock()
	if c.lastCommittedBlock == nil {
		return 0, errors.New("last committed block == nil")
	}
	return c.lastCommittedBlock.Header.BlockHeight, nil
}

// ####################################################################################################################
//                                       impls NetService for TBFT         (only use GetNodeUidByCertId() method)
// ####################################################################################################################
//NewNetServiceForTest
func NewNetServiceForTest() protocol.NetService {
	ctrl := gomock.NewController(nil)
	netService := mock.NewMockNetService(ctrl)

	netService.EXPECT().GetNodeUidByCertId(gomock.Any()).DoAndReturn(
		func(certId string) (string, error) {
			if nodeId, ok := mapper_certId_nodeId[certId]; ok {
				return nodeId, nil
			}
			return "", errors.New("certId is invalid")
		},
	).AnyTimes()

	return netService
}
