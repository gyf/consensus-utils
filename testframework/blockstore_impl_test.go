/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package testframework

import (
	"testing"

	"github.com/stretchr/testify/require"
)

//TestBlockStore
func TestBlockStore(t *testing.T) {
	blockStore := NewBlockChainStoreForTest()
	contractName := "testContractName"
	key := "key1"
	val := []byte("test1")
	blockStore.SetInitStates(contractName, key, val)
	val2, err := blockStore.ReadObject(contractName, []byte(key))
	require.Nil(t, err)
	require.Equal(t, val, val2)
}
