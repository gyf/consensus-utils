/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package wal_service

// WalWriteMode the mode of wal write
type WalWriteMode int

const (
	SyncWalWrite WalWriteMode = iota // write wal sync: 0
	AsyncWalWrite
	NonWalWrite
	WALWriteModeKey = "WAL_write_mode"
	WalDir          = "wal"
)

type ConsensusWalOptionFunc func(option *ConsensusWalOption)

// MarshalFunc the function which marshal data to bytes
// if there is error when marshal, the process will panic
type MarshalFunc func(data interface{}) []byte

type ConsensusWalOption struct {
	walWriteMode WalWriteMode
	walWritePath string
}

func NewDefaultConsensusWalOption() ConsensusWalOption {
	return ConsensusWalOption{
		walWriteMode: NonWalWrite, // default non write wal
	}
}

func WithWriteMode(walWriteMode WalWriteMode) ConsensusWalOptionFunc {
	return func(option *ConsensusWalOption) {
		option.walWriteMode = walWriteMode
	}
}

func WithWritePath(walWritePath string) ConsensusWalOptionFunc {
	return func(option *ConsensusWalOption) {
		option.walWritePath = walWritePath
	}
}
